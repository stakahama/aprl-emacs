#+title: "Literate programming" with org-babel: customizations
#+options: \n:t ^:nil
#+options: html-postamble:nil

* Documentation

https://org-babel.readthedocs.io
https://orgmode.org/mathjax/docs/html/index.html

(Note that many of the org-babel and referencing features - citeproc-org - does not work with pandoc.)

* Additional packages

- =poly-org=
- =poly-R=
- =ox-extra=
- =org-tempo=

* Setup

#+begin_src emacs-lisp :eval no
(custom-set-variables
 '(org-babel-load-languages (quote ((emacs-lisp . t) (R . t) (python . t))))
 '(org-confirm-babel-evaluate nil))
#+end_src

* Templates/completion

- easy templates (org-tempo)
- YASnippet
- CDLatex
- hippie-expand
- company-mode (optional)

* Setting local variables

Options:
- Define at top: 
  #+begin_src org
  # -*- org-html-link-org-files-as-html:nil; -*-
  #+end_src
- Use in premble: 
  #+begin_src org
  #+bind: org-html-link-org-files-as-html:nil
  #+end_src
- Use emacs-lisp code:
  #+begin_src org
  ,#+begin_src emacs-lisp :exports none :tangle no
  (set (make-local-variable org-html-link-org-files-as-html) nil)
  ,#+end_src
  #+end_src
- End block:
  #+begin_src org :eval no
  ,* File local variables :noexport:
  # local variables:
  # org-html-link-org-files-as-html: nil
  # end:
  #+end_src

Note: /not all variables can be set locally/.

For variables which need to be set as a non-buffer variable, you can [[https://stackoverflow.com/questions/23154306/org-mode-file-specific-functions][name the source block and call it from the file header]]. Contrived example:
#+begin_src org
#+name: org-org-link
,#+begin_src emacs-lisp :exports none :tangle no
(setq org-html-link-org-files-as-html nil)
,#+end_src
#+end_src
with this in the header:
#+begin_example
# eval: (org-sbe "org-org-link"); -*-
#+end_example

You will be prompted to allow this for future sessions; these variables will be stored in =safe-local-variable-values=, which can be /customized/ to delete later.

* Formatting
** Indentation

Turn off adaptation of indentation to outline levels. 

| Variable                           | Value |
|------------------------------------+-------|
| =org-adapt-indentation=            | =nil= |
| =org-indent-mode=                  | =nil= |
| =org-edit-src-content-indentation= | =0=   |

** SVG sizing

https://emacs.stackexchange.com/a/35716/2421

#+begin_src css :eval no
.org-svg { width: auto; }
#+end_src

** CSS

Define css in preamble.
#+begin_src org :eval no
,#+html_head: <link rel="stylesheet" type="text/css" href="/css/style.css"/>
#+end_src

** Strip bullets from table of contents

#+begin_src emacs-lisp :eval no
(defun org-html--toc-text-no-bullets (orig-fun &rest args)
  "Removes bullets from table of contents. Remove advice with (advice-remove 'org-html--toc-text #'org-html--toc-text-no-bullets)"
  (let ((toc-string (apply orig-fun args)))
    (replace-regexp-in-string "<ul>" "<ul style=\"list-style-type:none\">" toc-string () t)))

(advice-add 'org-html--toc-text :around #'org-html--toc-text-no-bullets)
#+end_src

* Unicode characters

=ucs-insert= (=C-x 8 RET=) [[[https://www.emacswiki.org/emacs/UnicodeEncoding][1]]][[[http://ergoemacs.org/emacs/emacs_n_unicode.html][2]]]:

| example | key binding |
|---------+-------------|
| é       | =C-x 8 'e=  |
| à       | =C-x 8 `a=  |
| î       | =C-x 8 ^a=  |
| ñ       | =C-x 8 ~n=  |
| ü       | =C-x 8 "u=  |

Press =C-x 8 C-h= to see all characters.

* Referencing sections
:PROPERTIES:
:CUSTOM_ID: referencing
:END:

Use properties tag and reference with =[[#label]]=. Generate Properties template with =C-c C-x p RET CUSTOM_ID RET {label}=.
#+begin_src org :eval no
,* Section heading
:PROPERTIES:
:CUSTOM_ID: label
:END:
#+end_src

* Automatic figure numbering

** COMMENT Generated with code blocks

R markdown automatically numbers figures that are generated. We can sort of emulate this in org-babel - inspiration taken from this [[https://stackoverflow.com/a/29101559/143476][post]]. The block needs =#+name= to be run. Currently, the problem is that figures accumulate even if code blocks are deleted. Clearing out figure directory (=fig-directory=) before doing a final run may be good practice.

#+begin_src emacs-lisp :eval no
#+name: fig-file
,#+begin_src emacs-lisp :exports none :tangle no
(set (make-local-variable 'fig-directory) 
     (concat 
      (file-name-sans-extension (file-name-nondirectory (buffer-file-name)))
      "_figs"))
(set (make-local-variable 'fig-num) 0)

(defun fig-file (&optional ext)
  "Generate figure file name. 'fig-directory' and 'fig-num' are dynamically scoped."
  (let ((ext (or ext "png")))
    (concat (file-name-as-directory fig-directory)
	    (format "f%d.%s" (incf fig-num) ext))))

(unless (file-exists-p fig-directory)
  (make-directory fig-directory))
,#+end_src
#+end_src

Example usage:
#+begin_example
~#+begin_src R :results graphics :file (fig-file) :units "in" :res 96 :width 8 :height 10~
#+end_example

** Generated with code blocks

R markdown automatically numbers figures that are generated. We can sort of emulate this in org-babel.

#+begin_src emacs-lisp :eval no
(set (make-local-variable 'fig-directory) 
     (replace-regexp-in-string "\\.org$" "_figs" (file-name-nondirectory (buffer-file-name))))
(set (make-local-variable 'fig-num) 0)
(set (make-local-variable 'fig-ext) "png")

(defun fig-file (&optional ext)
  "Generate figure file name. 'fig-directory', 'fig-num', 'fig-ext' are dynamically scoped"
  (let ((ext (or ext fig-ext)))
    (concat (file-name-as-directory fig-directory)
	    (format "f%d.%s" (incf fig-num) ext))))
#+end_src
The following code is also necessary to purge old figure files or create a new figure directory.
#+begin_src emacs-lisp :eval no
(if (file-exists-p fig-directory)
    (let* ((fullpath (file-name-as-directory (expand-file-name fig-directory)))
	   (filelist  (directory-files fullpath))
	   (x nil))
      (dolist (x filelist nil)
	(if (string-match (format "f\\([0-9]\\)+\\.%s" fig-ext) x)
	    (delete-file (concat fullpath x)))))
  (make-directory fig-directory))
#+end_src
Then apply to code blocks:
#+begin_src emacs-lisp :eval no
,#+begin_src R :results graphics file :file (fig-file) :res 96 :units "in" :width 7 :height 4
plot(1:10)
,#+end_src
#+end_src

** Copy-paste screenshots

Inspired from this [[https://stackoverflow.com/a/17438212/143476][post]].
#+begin_src emacs-lisp :eval no
,#+begin_src emacs-lisp :eval no-export :export no
(set (make-local-variable 'img-directory) 
     (file-name-as-directory (concat (file-name-sans-extension (buffer-file-name)) "_imgs")))

(defun my-org-screenshot (&optional arg)
  "Take a screenshot in memory and save into a directory 'img-directory' (a dynamically-scoped file directory name) by time/date and insert a link to this file. If prefix argument is provided (C-u), will be prompted for file name."
  (interactive "P") ;; "F"
  (let ((filename))
    (setq filename
	  (if arg
	      ;; expand-file-name required for pngpaste
	      (expand-file-name (read-file-name "Image name: " img-directory () 'confirm))
	    ;; (concat img-directory (make-temp-name "img_") ".png")
	    (concat img-directory (format-time-string "img_%Y%m%d_%H%M%S") ".png")))
    (princ filename)
    ;; (call-process "import" nil nil nil filename) ;; imagemagick
    (call-process "pngpaste" nil nil nil filename) ;; macOS
    (insert (format "[[./%s]]" (file-relative-name filename))))
  (org-display-inline-images))

(progn
  (use-local-map (copy-keymap org-mode-map))
  (local-set-key (kbd "C-c M-y") 'my-org-screenshot))

(unless (file-exists-p img-directory)
  (make-directory img-directory))

(setq org-image-actual-width nil)
,#+end_src
#+end_src

To set the visual size, do two things:

1. Set =org-image-actual-width=
   #+begin_src emacs-lisp :eval no
   (setq org-image-actual-width nil)
   #+end_src
2. Then, set width of each image
   #+begin_src emacs-lisp :eval no
   ,#+ATTR_ORG: :width 300
   #+end_src
3. Toggle inline images to reset (=C-c C-x C-v=)

* LaTeX/MathJax

For export of a document to HTML and LaTeX.

Note that the [[https://orgmode.org/manual/LaTeX-fragments.html][inline math expressions]] should be enclosed in =\(...\)= rather than =$...$=.

#+begin_quote
Text within the usual LaTeX math delimiters. To avoid conflicts with currency specifications, single =‘$’= characters are only recognized as math delimiters if the enclosed text contains at most two line breaks, is directly attached to the =‘$’= characters with no whitespace in between, and if the closing =‘$’= is followed by whitespace, punctuation or a dash. For the other delimiters, there is no such restriction, so when in doubt, use =‘\(...\)’= as inline math delimiters. 
#+end_quote


** Writing LaTeX in org-mode

*** CDLaTeX
:PROPERTIES:
:CUSTOM_ID: CDLaTeX
:END:


CDLaTeX already includes many completions:

#+begin_src emacs-lisp :eval no
(add-hook 'org-mode-hook 'turn-on-org-cdlatex)
#+end_src

| key  | environment |
|------+-------------|
| equ  | equation    |
| equ* | equation*   |
| ali  | align       |
| ali* | align*      |
| ite  | itemize     |

*Alternatively*, use =C-c {= to insert LaTeX environment.

CDLatex binds =_= and =^= to a function which generates =_{}= and =^{}= when writing latex. Disable with the following commands.
#+begin_src emacs-lisp
(org-defkey org-cdlatex-mode-map (kbd "_") #'self-insert-command) ;#'org-cdlatex-underscore-caret
(org-defkey org-cdlatex-mode-map (kbd "^") #'self-insert-command) ;#'org-cdlatex-underscore-caret
#+end_src
Additionally, ='= in the math environment initiates a sequence for specifying an accent/font. Escape single quote with =C-q '=.

*** YASnippets
:PROPERTIES:
:CUSTOM_ID: YASnippets
:END:


YASnippets: [[https://simpleit.rocks/lisp/emacs/adding-custom-snippets-to-yasnippet/][define custom snippet]] in ~/.emacs.d/snippets/org-mode/begin:
#+begin_src :eval no
# -*- mode: snippet -*-
# name: Latex environment, from https://stackoverflow.com/a/9083046/143476
# key: begin
# --
\begin{$1}
  `yas/selected-text`$0
\end{$1}
#+end_src

In either case, use =C-c '= to edit.

#+begin_src emacs-lisp :eval no
(setq org-highlight-latex-and-related '(latex))
#+end_src

Use also =C-c '= =(org-edit-special)= to narrow to region and edit.

** Using MathJax for HTML export

https://readthedocs.org/projects/mathjax/

Requirements to use LaTeX packages equivalents:
- connection to MathJax [[https://docs.mathjax.org/en/latest/start.html#using-a-content-delivery-network-cdn][CDN]]. See https://cdnjs.com/libraries/mathjax for servers.
- implementation in javascript ("extensions")

Set variables: =org-html-mathjax-options= and possibly =org-html-mathjax-template=. See examples: [[https://www.gnu.org/software/emacs/manual/html_node/org/Math-formatting-in-HTML-export.html][1]], [[https://orgmode.org/manual/Math-formatting-in-HTML-export.html][2]], [[https://emacs.stackexchange.com/questions/31271/siunitx-mathjax-org-mode-and-html-export][3]], [[https://stackoverflow.com/questions/49300667/which-mathjax-cdn-script-should-be-used][4]].

Can set settings [[https://www.gnu.org/software/emacs/manual/html_node/org/Math-formatting-in-HTML-export.html][within specific org files]].

#+begin_src emacs-lisp :eval no
(setq org-html-mathjax-options
      '((path "https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/MathJax.js?config=TeX-MML-AM_CHTML")
	(scale "100")
	(align "center")
	(font "TeX")
	(linebreaks "false")
	(autonumber "AMS")
	(indent "0em")
	(multlinewidth "85%")
	(tagindent ".8em")
	(tagside "right")
	(mathml t)))

(setq org-html-mathjax-template
              "
<script type=\"text/javascript\" src=\"%PATH\"></script>
<script type=\"text/javascript\">
<!--/*--><![CDATA[/*><!--*/
    MathJax.Hub.Config({
        jax: [\"input/TeX\", \"output/HTML-CSS\"],
        extensions: [\"tex2jax.js\",\"TeX/AMSmath.js\",\"TeX/AMSsymbols.js\",
                     \"TeX/noUndefined.js\", \"[Contrib]/siunitx/siunitx.js\", \"[Contrib]/mhchem/mhchem.js\"],
        tex2jax: {
            inlineMath: [ [\"\\\\(\",\"\\\\)\"] ],
            displayMath: [ ['$$','$$'], [\"\\\\[\",\"\\\\]\"], [\"\\\\begin{displaymath}\",\"\\\\end{displaymath}\"] ],
            skipTags: [\"script\",\"noscript\",\"style\",\"textarea\",\"pre\",\"code\"],
            ignoreClass: \"tex2jax_ignore\",
            processEscapes: false,
            processEnvironments: true,
            preview: \"TeX\"
        },
        TeX: {extensions: [\"AMSmath.js\",\"AMSsymbols.js\",  \"[Contrib]/siunitx/siunitx.js\", \"[Contrib]/mhchem/mhchem.js\"]},
        showProcessingMessages: true,
        displayAlign: \"%ALIGN\",
        displayIndent: \"%INDENT\",

        \"HTML-CSS\": {
             scale: %SCALE,
             availableFonts: [\"STIX\",\"TeX\"],
             preferredFont: \"TeX\",
             webFont: \"TeX\",
             imageFont: \"TeX\",
             showMathMenu: true,
        },
        MMLorHTML: {
             prefer: {
                 MSIE:    \"MML\",
                 Firefox: \"MML\",
                 Opera:   \"HTML\",
                 other:   \"HTML\"
             }
        }
    });
/*]]>*///-->
</script>")
#+end_src

To [[https://jupyter-contrib-nbextensions.readthedocs.io/en/latest/nbextensions/equation-numbering/readme.html][enable]] [[https://docs.mathjax.org/en/latest/input/tex/eqnumbers.html][autonumbering]], include the following field (=equationNumbers=) to the =TeX= element above:
#+begin_src javascript
MathJax.Hub.Config({
    TeX: { equationNumbers: { autoNumber: "AMS" } }
});
#+end_src


[[https://stackoverflow.com/a/51888146/143476][Example usage]]:
#+begin_src org :eval no
#+begin_export html
$$
\require{\mhchem}
\require{\siunitx}
\newcommand{\wav}{\tilde{\nu}}
$$
#+end_export
#+end_src
Enclose in =#+begin_export html...#+end_export= so that it is not exported to latex as it can conflict with latex headers. Should also be able to specify packages in =#+html_mathjax= but I have not had success with it.

#+begin_export html
$$
\require{\mhchem}
\require{\siunitx}
\newcommand{\wav}{\tilde{\nu}}
$$
#+end_export

Then =\ce=, =\si=, =\wav=, etc. need to be enclosed in a math environment in contrast to LaTeX. E.g.:
- =\(\ce{HNO3}\)= \to \(\ce{HNO3}\)
- =\(\si{\cm^{-1}}\)= \to \(\si{\cm^{-1}}\)
- =\(\wav\)= \to \(\wav\)

Markup/code that should only be exported to latex can be enclosed in =#+begin_export latex...#+end_export=. E.g., =#+begin_export latex \hrulefill #+end_export= to separate TOC from main document. Note that =#+begin_latex...#+end_latex= can be used for HTML export but will generate a =\begin{latex}...\end{latex}= environment in latex and return an error when compiling.

* Additional customizations

** Citing references with citeproc-org

[[https://github.com/andras-simonyi/citeproc-org][citeproc-org]] is based on [[https://github.com/jkitchin/org-ref][org-ref]] but allows [[https://github.com/citation-style-language/styles][CSL file definitions]]. Enable with
#+begin_src emacs-lisp :eval no
(require 'citeproc-org) 
(citeproc-org-setup)
#+end_src

In addition to the statements required for =org-ref=, the preamble in each document can define a CSL file:
#+begin_src org :eval no
#+csl_style: american-geophysical-union.csl
#+end_src

To speed up (cannot be local variable, apparently):
#+begin_src emacs-lisp :eval no
(setq org-ref-show-broken-links nil)
#+end_src

Remove "Bibliography" header before reference list. This apparently cannot be made a buffer local variable.
#+begin_src emacs-lisp :eval no
(setq citeproc-org-html-bib-header "")
#+end_src
Set back to default value with 
#+begin_src emacs-lisp :eval no
(setq citeproc-org-html-bib-header (eval (car (get 'citeproc-org-html-bib-header 'standard-value))))
#+end_src

By default, only =citealt= doesn't use parentheses; =citet= behaves as =citep=. To suppress parentheses for =citet=:
#+begin_src emacs-lisp
(setq citeproc-org-suppress-affixes-cite-link-types '("citet" "citealt"))
#+end_src

* Key bindings

From [[https://orgmode.org/manual/Structure-Templates.html][easy-templates]], some commonly-used structures:

| shortcut  | structure |
|-----------+-----------|
| =<C[TAB]= | comment   |
| =<e[TAB]= | example   |
| =<E[TAB]= | export    |
| =<q[TAB]= | quote     |
| =<s[TAB]= | src       |

For CDLaTeX, see Section [[#CDLaTeX]].
For YASnippet, see Section [[#YASnippets]].
Generate Properties template with =C-c C-x p=, see Section [[#referencing]].
