#+TITLE: Shell operations

For most heavy terminal work, I use the OS's terminal (particularly
for establishing ssh and running emacs on the remote machine through
the terminal). Here's a few features I find lacking in each of the options:
- Shell: can't ssh
- Eshell: can't ssh, no input redirection (but has many powerful features which integrates it with Emacs)
- Term, ansi-term, multi-term: Invoking emacs on remote computer; some conflicts with usual key commands
- Tramp-mode: can be slow.

In shell and eshell you cannot use =$ emacs --script= to run a script (or so it seems).

On a separate note, you can use =Ctrl-x Ctrl-e= on the bash terminal to open up an editor and copy-paste a long command (that editor can be configured to be emacs).

* Shell
I primarily use ~shell~ for moving/copying/transfering files and
running executables.
+ Environment variables and aliases from my
  ~.bash_profile~ are imported in ~.emacs.d/init_bash.sh~.
#+BEGIN_SRC sh
# export PS1='\h:\w$ '
source ~/.bash_profile
#+END_SRC
+ To start multiple shells: ~C-u M-x shell~.
+ To use ~TAB~ for smart auto-completion, add ~(require 'shell)~.
+ If you hop directories using aliases, the shell will sometimes not
  catch up. Use ~(dirs)~ - I have this mapped to ~C-c d~ in
  shell-mode. Can use more aggressive tracking.

My shell configurations are [[https://bitbucket.org/stakahama/aprl-emacs/raw/03035b4d43e366b3f7cc0db9c12b3ca698c35452/lisp/aprl-shell.el][here]].

Some key bindings I use frequently:
| task                                         | keybinding  |
|----------------------------------------------+-------------|
| invoke dirs                                  | ~C-c d~     |
| change directory to other window's directory | ~C-u C-c c~ |
| clear shell                                  | ~C-c l~     |

* Eshell

I understand ~eshell~ is also powerful and traversing through remote
directories is easy as ~$ cd /ssh:user@host:dirname/~. Also supports evaluation of emacs lisp expressions. However, eshell does not support input redirection.

In [[https://bitbucket.org/stakahama/aprl-emacs/raw/03035b4d43e366b3f7cc0db9c12b3ca698c35452/lisp/aprl-shell.el][aprl-config]], ~C-u C-c c~ is bound to change pwd to other window's directory, as above (in shell-mode).

* Term-family
~term~, ~ansi-term~, and ~multi-term~ are also supposed to be
powerful, even spawning emacs within emacs, but may require setting
the environment variable (=TERM=) on the remote machine.
+ Default =TERM= is =xterm-color= in both OS X and Ubuntu; emacs works
  in term-mode works on my OS X (local machine) without setting =TERM=
  variable but not on my Ubuntu Linux (remote machine).
+ When logging onto my Ubuntu Linux from OS X, I can set =export
  TERM=vt100 && emacs= and this will enable me to run emacs within a
  term shell. However, this is not the prettiest option.
+ Term-mode has [[http://www.gnu.org/software/emacs/manual/html_node/emacs/Term-Mode.html][modes]] in the way that vi has modes: line-mode and
  char-mode. "In char mode, each character is sent directly to the
  inferior subshell, except for the Term escape character, normally
  =C-c=."
+ In char mode, =C-x C-k= kills emacs buffer running in term; in line
  mode =C-x C-k= offers to kill the super-emacs process running
  term-mode. To exit emacs running in term, =M-x
  save-buffers-kill-emacs= from char mode.

=term= and =ansi-term= are [[https://lists.gnu.org/archive/html/help-gnu-emacs/2007-12/msg00199.html][now similar]], apparently. More info [[http://www.masteringemacs.org/articles/2010/11/01/running-shells-in-emacs-overview/][here]].

My multi-term configuration is [[https://bitbucket.org/stakahama/aprl-emacs/raw/03035b4d43e366b3f7cc0db9c12b3ca698c35452/lisp/aprl-multi-term.el][here]].

* Tramp

~Tramp-mode~ can also be used to edit files located on a remote
machine: open file with ~/scp:username@host#port:/~ (#port is
optional) and all the customizations for the emacs on local machine
will be available. This is useful for significant edits for files, but
since file retrieval takes a little time not ideal for quick edits
(probably faster to fire up emacs in terminal on remote machine).


#+HTML:<br>
#+HTML:<hr>
#+INCLUDE: "./emacs-sitemap.org"
#+HTML:<br>
