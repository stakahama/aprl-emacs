#+TITLE: Buffer management

Though I usually use ~ido-mode~ to cycle through buffers, I occasionally
use ~ibuffer~ to see what buffers I have open, and close ones I am no
longer using.

| task                 | keybinding |
|----------------------+------------|
| mark buffer          | ~m~        |
| unmark buffer        | ~u~        |
| mark unsaved buffers | ~*u~       |
| save marked buffer   | ~S~        |
| close marked buffers | ~D~        |
| unmark all           | ~**~       |
| mark by mode name    | ~%m~       |
| mark by file path    | ~%f~       |

http://xahlee.org/emacs/effective_emacs.html

sorting:
| task                                      | keys           |
|-------------------------------------------+----------------|
| Rotate between the various sorting modes. | <code>,</code> |
| Reverse the current sorting order.        | ~s i~          |
| Sort the buffers lexicographically.       | ~s a~          |
| Sort the buffers by the file name.        | ~s f~          |
| Sort the buffers by last viewing time.    | ~s v~          |
| Sort the buffers by size.                 | ~s s~          |
| Sort the buffers by major mode.           | ~s m~          |

(electric-buffer-list is also electrifying)

#+HTML:<br>
#+HTML:<hr>
#+INCLUDE: "./emacs-sitemap.org"
#+HTML:<br>
