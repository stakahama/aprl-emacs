#+TITLE: evil-mode in emacs

#+BEGIN_SRC emacs-lisp
(require 'evil)
(define-key evil-motion-state-map (kbd "C-o") 'evil-execute-in-normal-state)
#+END_SRC

basic movement: h (back), l (forward), j (up), k (down).

| unit      | backward | forward |
|-----------+----------+---------|
| word      | b        | e       |
| sentence  | (        | )       |
| paragraph | {        | }       |
| line      | 0        | $       |
| file      | 1G       | G       |

| keybinding | function                 |
|------------+--------------------------|
| H          | move to top of screen    |
| M          | move to middle of screen |
| L          | move to bottom of screen |

visual commands

cut and paste

insert mode
