#+TITLE: Other Utilities

* Viewing delimited files

Org-mode option 1

1. set buffer to org-mode (=M-x org-mode=)
2. highlight entire buffer (=M-x mark-whole-buffer=)
3. =C-c |=

Org-mode option 2

1. create new buffer 
2. set buffer to org mode (=M-x org-mode=)
3. =M-x org-table-import= and select file


Csv-mode

1. set buffer to csv-mode (=M-x csv-mode=)
2. =M-x csv-align-fields=

In all options, turn off visual-line-mode so that =C-a=, =C-e= goes to beginning nad end of line, respectively.
