#+TITLE: Programming in Emacs Lisp


There are several aspects of emacs lisp that are peculiar when compared to many other modern languages.

* Lisp-2

The same symbol can be used for a function and a variable simultaneously. ([[https://www.gnu.org/software/emacs/manual/html_node/elisp/Function-Cells.html][ref]])
#+BEGIN_SRC emacs-lisp
(defun foo () 1)	;; function

(setq foo 2)		;; variable

(foo)			;; => 1

foo			;; => 2
#+END_SRC

The reason why two definitions can exist simultaneously for a single symbol (in a single namespace) is that each symbol can be conceptualized as having a "function cell" and a "value cell". In the example below, we show how two separate functions can be represented by the same symbol. Note that when a function is assigned to the value cell, it must be invoked using different syntax.
#+BEGIN_SRC emacs-lisp
(defun bar () 1)		;; assign to function cell

(setq bar (lambda () 2))	;; assign to value cell

(bar)				;; => 1

bar				;; => (lambda nil 2)

(funcall bar)			;; => 2
#+END_SRC

Deleting a variable or checking the existence of a variable requires that the function and value cells of the symbol are addressed separately.
#+BEGIN_SRC emacs-lisp
(fmakunbound 'bar)		;; => bar

(fboundp 'bar)			;; => nil

(bar)				;; => Symbol's function definition is void: bar

(makunbound 'bar)		;; => bar

(boundp 'bar)			;; => nil

bar				;; => Symbol's value as variable is void: bar
#+END_SRC

There is an alternate (and perhaps unconventional) way to define and call functions that mirror the way we handle values.
#+BEGIN_SRC emacs-lisp
(fset 'bar (lambda () 1))	;; => (lambda nil 1)

(set 'bar (lambda () 2))	;; => (lambda nil 2)

(funcall 'bar)			;; => 1

(funcall bar)			;; => 2
#+END_SRC

* Dynamic scoping


An important thing to note is the dynamic scoping rules of traditional emacs lisp. Lexical scoping is permitted in some instances:

- using =lexical-let=
- set buffer-local variable =lexical-binding= to non-nil (since Emacs 24).

An example contrasting dynamic and lexical scoping is taken from the [[https://www.emacswiki.org/emacs/DynamicBindingVsLexicalBinding][EmacsWiki]]. The example is modified to use =cl-flet= and store the function definition in the symbol =f='s function cell, rather its value cell. Note that the value of a free symbol in dynamic scoping is defined by the environment in which the function is invoked, and not in that which it is defined.

#+BEGIN_SRC emacs-lisp
(require 'cl)

;; dynamic scoping
(let ((a 1))                    ; binding (1)
  (cl-flet ((f () a))
    (let ((a 2))                ; binding (2)
      (f))))
;; => 2

;; lexical scoping
(lexical-let ((a 1))            ; binding (1)
  (cl-flet ((f () a))
    (lexical-let ((a 2))        ; binding (2)
      (f))))
;; => 1

;; alternatively,
(setq lexical-binding t)        ; set for entire buffer
(let ((a 1))                    ; binding (1)
  (cl-flet ((f () a))
    (let ((a 2))                ; binding (2)
      (f))))
;; => 1
(setq lexical-binding nil)      ; original value
#+END_SRC

* t vs. nil

Boolean values are =t= and =nil=. [[https://www.gnu.org/software/emacs/manual/html_node/elisp/nil-and-t.html][(ref]])

All of the following statements are true. Note that =0= and negative values are also true, in contrast with many other languages.

#+BEGIN_SRC emacs-lisp
(let ((a t))
  (if a "true" "false"))
;; => "true"

(let ((a 0))
  (if a "true" "false"))
;; => "true"

(let ((a -1))
  (if a "true" "false"))
;; => "true"
#+END_SRC

=f= is not false -- there is no symbol named =f= defined in the language.
#+BEGIN_SRC emacs-lisp
(let ((a f))
  (if a "true" "false"))
;; => Symbol's value as variable is void: f
#+END_SRC


Both of the following statements are false. =nil= is equivalent to the empty list.
#+BEGIN_SRC emacs-lisp
(let ((a nil))
  (if a "true" "false"))
;; => "false"

(let ((a ()))
  (if a "true" "false"))
;; => "false"
#+END_SRC

* Creating sequences by quote vs. list

=quote= (shorthand: ') accepts a single argument and returns the object (symbol or expression) unevaluated. =list= returns a sequence of evaluated objects. Quoted numbers evaluate to themselves so the following expressions are equivalent:
#+BEGIN_SRC emacs-lisp
(mapcar '1+ (list 1 2)) ;; => (2 3)

(mapcar '1+ '(1 2))     ;; => (2 3)
#+END_SRC

However, for variables this is not the case.
#+BEGIN_SRC emacs-lisp
(let ((a 1))
  (list a 2))
;; => (1 2)

(let ((a 1))
  '(a 2))
;; => (a 2)
#+END_SRC

'(a 1) evaluates to =(list 'a '1)=:
#+BEGIN_SRC emacs-lisp
(equal '(a 1) (list 'a '1)) ;; => t
#+END_SRC

Therefore, the following two expressions produce different results.
#+BEGIN_SRC emacs-lisp
(let* ((a 1)
       (pair (list a 2)))
  (mapcar '1+ pair))
;; => (2 3)

(let* ((a 1)
       (pair '(a 2)))
  (mapcar '1+ pair))
;; => Wrong type argument: number-or-marker-p, a
#+END_SRC

Perhaps there is a better solution, but a call to =eval= should be necessary at some point for the latter example.
#+BEGIN_SRC emacs-lisp
(let* ((a 1)
       (pair '(a 2)))
  (mapcar '1+ (eval (cons 'list pair))))
;; => (2 3)
#+END_SRC

Note that we can also use a backquote to partially evaluate select elements of a quoted list.
#+BEGIN_SRC emacs-lisp
(let* ((a 1)
       (pair `(,a 2)))
  (mapcar '1+ pair))
;; => (2 3)
#+END_SRC

* Lists

Building quoted lists:
#+BEGIN_SRC emacs-lisp
(cons '+ '(1 2))
(append '(+) '(1 2))
#+END_SRC

Evaluating functions:
#+BEGIN_SRC emacs-lisp
(funcall '+ 1 2)
(apply '+ (list 1 2))
(eval '(+ 1 2))
#+END_SRC

For the following examples, let us use this variable:
#+BEGIN_SRC emacs-lisp
(setq triple (list 1 2 3))
#+END_SRC

Extracting elements (note lists are zero-indexed for =nth=):
#+BEGIN_SRC emacs-lisp
(car triple)				;; => 1
(cdr triple)				;; => (2 3)
(butlast triple)			;; => (1 2)
(last triple)				;; => (3)
(car (last triple))			;; => 3
(car (reverse triple))			;; => 3
(nth (1- (length triple)) triple)	;; => 3
#+END_SRC

Multiple assignment can be accomplished using =destructuring-bind=:
#+BEGIN_SRC emacs-lisp
(let (x y z)
  (destructuring-bind (x y z) triple
    (+ y z)))
;; => 5
#+END_SRC

* Other structures

[[http://ergoemacs.org/emacs/elisp_vector.html][Sequences]] include
- lists (recursive structures)
- vectors (1-D of type: vector, string, char-table, bool-vector)

[[http://ergoemacs.org/emacs/elisp_hash_table.html][Hash tables]] can be implemented as
- association lists ("alist", ordered, possibly duplicate keys)
- hash tables (unordered, unique entries, fast)
