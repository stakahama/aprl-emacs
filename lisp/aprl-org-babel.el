
;;------------------------------------------------------------------------------
;;
;;    This file is part of aprl-emacs.
;;    
;;    aprl-emacs is free software: you can redistribute it and/or modify
;;    it under the terms of the GNU General Public License as published by
;;    the Free Software Foundation, either version 3 of the License, or
;;    (at your option) any later version.
;;    
;;    aprl-emacs is distributed in the hope that it will be useful,
;;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;    GNU General Public License for more details.
;;    
;;    You should have received a copy of the GNU General Public License
;;    along with aprl-emacs.  If not, see <http://www.gnu.org/licenses/>.
;;
;;------------------------------------------------------------------------------

;;;_* libraries


(require 'org-tempo) ;; for easy templates for newer org versions

;; ---
;; may need to install outside of Elpa
(condition-case nil
    (require 'org-latex)
  (error nil))

(condition-case nil
    (require 'ox-extra)
  (error nil))
;; ---

;; (condition-case nil
;;     (progn
;;       (require 'org-ref)
;;       (require 'citeproc-org) 
;;       (citeproc-org-setup))
;;   (condition-case nil
;;       (require 'ox-pandoc)
;;     (error nil))
;;   (error nil))

(require 'org-ref)

(condition-case nil
    (require 'ob-async)
  (error nil))

(condition-case nil
    (progn
      (require 'poly-org)
      (require 'poly-R))
  (error nil))

;;;_* cdlatex

(require 'cdlatex)
;; (load (aprl-path-join (aprl-search-package 'cdlatex "~/.emacs.d/site-lisp") "cdlatex"))

;; http://www.mfasold.net/blog/2009/02/using-emacs-org-mode-to-draft-papers/
;; http://www.clarkdonley.com/blog/2014-10-26-org-mode-and-writing-papers-some-tips.html
(add-hook 'org-mode-hook 'turn-on-org-cdlatex)
;; (add-hook 'org-mode-hook 'wc-mode)

(org-defkey org-cdlatex-mode-map (kbd "_") #'self-insert-command) ;#'org-cdlatex-underscore-caret
(org-defkey org-cdlatex-mode-map (kbd "^") #'self-insert-command) ;#'org-cdlatex-underscore-caret

;;;_* export

(defun org-html--toc-text-no-bullets (orig-fun &rest args)
  "Removes bullets from table of contents. Remove advice with (advice-remove 'org-html--toc-text #'org-html--toc-text-no-bullets)"
  (let ((toc-string (apply orig-fun args)))
    (replace-regexp-in-string "<ul>" "<ul style=\"list-style-type:none\">" toc-string () t)))

(advice-add 'org-html--toc-text :around #'org-html--toc-text-no-bullets)

;;;_* variables

;; (setq org-highlight-latex-and-related '(latex script entities))
(setq org-highlight-latex-and-related '(latex))
(setq citeproc-org-html-bib-header "")
(setq citeproc-org-suppress-affixes-cite-link-types '("citet" "citealt"))
(setq org-ref-show-broken-links nil)

(setq org-adapt-indentation nil)
(setq org-indent-mode nil)
(setq org-edit-src-content-indentation 0)

;; (with-eval-after-load 'org (setq org-startup-indented nil))
;; (setq org-startup-indented nil)
(setq org-adapt-indentation nil)

(custom-set-variables
 '(org-babel-load-languages (quote ((emacs-lisp . t) (R . t) (python . t))))
 '(org-confirm-babel-evaluate nil))

