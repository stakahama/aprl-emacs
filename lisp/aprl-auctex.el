
;;------------------------------------------------------------------------------
;;
;;    This file is part of aprl-emacs.
;;    
;;    aprl-emacs is free software: you can redistribute it and/or modify
;;    it under the terms of the GNU General Public License as published by
;;    the Free Software Foundation, either version 3 of the License, or
;;    (at your option) any later version.
;;    
;;    aprl-emacs is distributed in the hope that it will be useful,
;;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;    GNU General Public License for more details.
;;    
;;    You should have received a copy of the GNU General Public License
;;    along with aprl-emacs.  If not, see <http://www.gnu.org/licenses/>.
;;
;;------------------------------------------------------------------------------


;;;_* Load

;;;_ . AUCTeX and preview-latex

;; (require 'tex)
;; (require 'preview)
;; (if (eq system-type 'windows-nt)
;;     (require 'tex-mik))

;;;_ . reftex

(require 'reftex)
(setq reftex-plug-into-AUCTeX t)

;;;_ . latexmk

(require 'auctex-latexmk)
(auctex-latexmk-setup)
(setq auctex-latexmk-inherit-TeX-PDF-mode t)

;;;_* Associate extensions

;; about modes:
;; - TeX-mode (Plain TeX, LaTeX, DocTeX, SliTeX) by AucTeX
;; - tex-mode (plain tex, latex, doctex, slitex) by emacs defaut

;; AucTeX overrides names of default tex modes
;; Normally, "mode: latex" at bottom of .tex file indicates latex-mode
;; Below, latex-mode is enabled for all .tex files by default

(add-to-list 'auto-mode-alist '("\\.tex$" . LaTeX-mode))

;; Add standard Sweave file extensions to the list of files recognized
;; by AUCTeX.
(setq TeX-file-extensions
      '("Snw" "Rnw" "snw" "rnw" "tex" "sty" "cls" "ltx" "texi" "texinfo" "dtx"))


;;;_* Configuration settings

(setq font-latex-fontify-script nil)
(setq font-latex-fontify-sectioning 'color)
(setq-default TeX-master nil)
;; (setq-default TeX-engine 'xetex)

(when (eq system-type 'darwin)
  
  ;; see also
  ;; http://www.stefanom.org/setting-up-a-nice-auctex-environment-on-mac-os-x/

  ;; Minimal OS X-friendly configuration of AUCTeX. Since there is no
  ;; DVI viewer for the platform, use pdftex/pdflatex by default for
  ;; compilation. Furthermore, use 'open' to view the resulting PDF.
  ;; Until Preview learns to refresh automatically on file updates, Skim
  ;; (http://skim-app.sourceforge.net) is a nice PDF viewer.

  (setq TeX-PDF-mode t)

  ;; PDF viewer
  (let* ((pdfviewer "Preview"))
    (setq TeX-output-view-style
	  `(("^dvi$" "^xdvi$" "xdvi %dS %d")
	    ("^dvi$" "." "open %dS %d")
	    ;;("^pdf$" "." "open %o")
	    ("^pdf$" "." ,(format "open -a \"%s\" %%o" pdfviewer))
	    ("^html?$" "." "open %o")))
    (push
     `("View"
       ,(format "open -a \"%s\" %%s.pdf" pdfviewer)
       TeX-run-discard-or-function t t
       :help "Run Viewer")
     TeX-command-list))

  ;; modify TeX-command-list
  (push
   '("latexmk" "latexmk -pdf %s" TeX-run-TeX nil t
     :help "Run latexmk on file")
   TeX-command-list)

  (push 
   '("XeLaTeX" "%`xelatex%(mode)%' %t" TeX-run-TeX nil t)
   TeX-command-list)
 
)

;;;_* Functions

;;;_ . libraries

(require 'aprl-latex)
(require 'aprl-utils)

;;;_ . clean

(defun aprl-TeX-clean (&optional arg)
  "Clean auxiliary files (TeX-clean). Additionally, when prefix argument is... 0: delete both output files and biber cache, 1: delete output files (TeX-clean t), else: delete biber cache but keep output files."
  (interactive "P")
  ;; cl-flet doesn't work(?)
  (flet ((rm-biber-cache ()
			 (delete-directory
			  (aprl-trim-string (shell-command-to-string "biber --cache"))
			  t)))
	 ;; (match-fdb-latexmk (x)
	 ;; 		    (string-equal "fdb_latexmk" (file-name-extension x))))
    (let (;; do not ask for confirmation
	  (TeX-clean-confirm nil)
	  ;; local variables
	  (flist-preclean nil)
	  (flist-postclean nil)
	  (file-fdb nil))
      ;; all files
      (setq flist-preclean (directory-files "."))
      ;; do the cleaning
      (cond ((eq 0 arg)
	     ;; remove interm. files + output files + biber cache
	     (TeX-clean t)
	     (rm-biber-cache))
	    ((eq 1 arg)
	     ;; remove interm. files + output files
	     (TeX-clean t))
	    ((bound-and-true-p arg)
	     ;; remove interm. files + biber cache
	      (TeX-clean)
	      (rm-biber-cache))
	    (t
	     ;; remove interm. files
	     (TeX-clean)))
      ;; remaining files
      (setq flist-postclean (directory-files "."))
      ;; need to delete ".fdb_latexmk"
      (setq file-fdb (concat (TeX-master-file) ".fdb_latexmk"))
      (when (member file-fdb flist-postclean)
	(delete-file file-fdb)
	(delete file-fdb flist-postclean))
      ;; print deleted files
      (message "deleted: %s"
	       (mapconcat 'identity
			  (cl-set-difference flist-preclean
					     flist-postclean
					     :test #'string-equal)
			  " ")))))

;;;_ . master designation

(defun aprl-TeX-master-name (master &optional test)
  (interactive)
  (let ((test (or test t)))
    (if (equal master test)
	(file-name-nondirectory (buffer-file-name))
      (replace-regexp-in-string "\"" "" master))))

(defun aprl-TeX-reset-master ()
  "Sometimes there is a problem that 'TeX-master becomes nil but TeX-local-master-p is still set. This resets the value of the 'TeX-master file value."
  (interactive)
  ;; (when (and (TeX-local-master-p) (eq nil TeX-master))
  (save-excursion
    (save-restriction
      (widen)
      (goto-char (point-max))
      (when (re-search-backward "^%%% TeX-master:[ \t]*\\([t]\\|\"[^ \t\n]+\"\\)" nil t)
	(let (master)
	  (setq master (match-string-no-properties 1))
	  (setq TeX-master (aprl-TeX-master-name master "t"))))))
  (message "\"%s\"" TeX-master))

;;;_ . doc view for PDFs

(defun aprl-TeX-pdf-doc-view ()
  (interactive)
  (let* ((master-file (replace-regexp-in-string "\.tex$" "" (aprl-TeX-master-name TeX-master)))
	 (pdf-file (format "%s.pdf" master-file)))
    (find-file-other-window pdf-file)))

;;;_ . window splitting

(defun TeX-recenter-output-buffer-split-window (&optional line)
  (let ((n (length (window-list))))
    (when (< n 2)
      (if (< (window-total-width) split-width-threshold)
	  (split-window-below)
	(split-window-right)))))

;; (defun reftex-toc-split-windows-direction (&optional REBUILD REUSE)
;;   (cl-flet ((dround (lambda (x d)
;; 		      (let ((z (expt 10 d)))
;; 			(/ (fround (* z x)) z)))))
;;     (let ((wf (frame-width))				; width (frame)
;; 	  (wm 80.0)					; width (max horizontal)	  
;; 	  (fr reftex-toc-split-windows-fraction))	; fraction (max)
;;       (if (> wf split-width-threshold)
;; 	  (progn
;; 	    (setq-local reftex-toc-split-windows-horizontally t)
;; 	    (setq-local reftex-toc-split-windows-fraction (max fr (dround (/ wm wf) 1))))
;; 	(setq-local reftex-toc-split-windows-horizontally nil)))
;;     (delete-other-windows)))

(defun reftex-toc-split-windows-direction (&optional REBUILD REUSE)
  (setq-local reftex-toc-split-windows-horizontally
	      (if (< (frame-width) split-width-threshold) nil t))
  (delete-other-windows))

;;;_* Hooks, add-advice

;;;_ . keybindings

(add-hook 'LaTeX-mode-hook (lambda ()
			     
			     ;; from aprl-auctex-2.el
			     ;; C-c d is too close to "C-c C-d" (TeX-save-document)
			     (local-set-key (kbd "C-c D") 'aprl-TeX-clean)
			     (local-set-key (kbd "C-c l") 'aprl-TeX-pdf-doc-view)

			     ;; from aprl-latex.el
			     (local-set-key (kbd "C-c e") 
					    (LaTeX-enclose-expression "$"))
			     (local-set-key (kbd "C-c r")
					    'LaTeX-wrap-environment-around-thing-or-region)
			     (local-set-key (kbd "C-c j") 
					    'LaTeX-insert-item-no-newline)))


;;;_ . other hooks

;; cursor-type
(add-hook 'LaTeX-mode-hook '(lambda () (setq-local cursor-type 'bar)))

;; make latexmk the default			   
(add-hook 'TeX-mode-hook (lambda () (setq TeX-command-default "latexmk")))

;;;_ . modes

;; reftex, outline-minor-mode
(add-hook 'LaTeX-mode-hook 'turn-on-reftex)

(add-hook 'LaTeX-mode-hook 'outline-minor-mode)

;;;_ . advice

(advice-add 'TeX-recenter-output-buffer :before #'TeX-recenter-output-buffer-split-window)

(advice-add 'reftex-toc :before #'reftex-toc-split-windows-direction)
